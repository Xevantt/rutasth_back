import express from "express";
import cors from 'cors'

const app = express();

//settings
app.set('port', process.env.PORT || 3000);

app.use(express.json())
app.use(express.urlencoded({ extended: true }))
/*
//Configurar cabeceras y cors
app.use((req, res, next) => {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Headers', 'Authorization, X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Allow-Request-Method');
    res.header('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, DELETE');
    res.header('Allow', 'GET, POST, OPTIONS, PUT, DELETE');
    next();
});
*/
const config = {
    application: {
        server: [
            {
                origin: 'https://pruebafab-kros.000webhostapp.com/',
                credencials: true
            },
            {
                origin: 'https://rutas-th.000webhostapp.com/',
                credencials: true
            },
            {
                origin: 'http://rutas-th.000webhostapp.com/',
                credencials: true
            }
        ]
    }
}

// app.use(cors())

app.use(cors(
    config.application.server
));
module.exports = app;