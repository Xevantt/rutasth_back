import connection from '../settings/dbConection'

//Mostrar los lugares de la categoria origen
export const getadminLugarOrigen = (request, response) => {
    //evalua
    connection.query("select * from lugar where tipo like 'O' order by Nombre asc", (err, result) => {
        if (err) {
            response.json(err)
        } else {
            response.json(result)
        }
    })
}
//Mostrar los lugares de la categoria destino
export const getadminLugarDestino = (request, response) => {
    //evalua
    connection.query("select * from lugar where tipo like 'D' order by Nombre asc", (err, result) => {
        if (err) {
            response.json(err)
        } else {
            response.json(result)
        }
    })
}
//llama a todas las combis
export const getadminCombiTrayecto = (request, response) => {
    //evalua
    connection.query("select * from combi", (err, result) => {
        if (err) {
            response.json(err)
        } else {
            response.json(result)
        }
    })
}
//----------------------------------------------------------------------------------
//Método POST admin
export const postadminTrayecto = (request, response) => {
    const { id_origen, id_destino, id_combi } = request.body;
    connection.query(
        `INSERT INTO trayecto(id_origen, id_destino, id_combi) VALUES(${id_origen}, ${id_destino}, ${id_combi})`,
        (err) => {
            if (err) console.log(err);
            response.json('Trayectoria insertada');
        }
    );
};
//llama a todas la relacion entre lugares y las combis por medio de las ID por las params
export const getadminIDtrayecto = (request, response) => {
    //evalua
    connection.query("select l1.Nombre as origen, l2.Nombre as destino, c.NumeroC as numC, c.NombreC as nombreC, t.id as id from trayecto t inner join lugar l1 on l1.id_L=t.id_origen inner join lugar l2 on l2.id_L=t.id_destino inner join combi c on c.id_C= t.id_combi order by id  desc limit 15",
        (err, result) => {
            if (err) {
                response.json(err)
            } else {
                response.json(result)
            }
        }
    )
}
//Delete
export const deleteAdminTrayecto = (request, response) => {
    const { id } = request.params
    connection.query(
        ` DELETE FROM trayecto WHERE id=${id}`,
        (err) => {
            if (err) return response.json({ status: false, info: 'El trayecto no se pudo eliminar', error: err })
            response.json({ status: true, info: '¡Trayecto eliminado!' })
        }
    )

}