import connection from '../settings/dbConection'

//Mostrar los lugares de la categoria origen
export const getLugarOrigen = (request, response) => {
    //evalua
    connection.query("select * from lugar where tipo like 'O' order by nombre asc", (err, result) => {
        if (err) {
            response.json(err)
        } else {
            response.json(result)
        }
    })
}
//Mostrar los lugares de la categoria destino
export const getLugarDestino = (request, response) => {
    //evalua
    connection.query("select * from lugar where tipo like 'D' order by nombre asc", (err, result) => {
        if (err) {
            response.json(err)
        } else {
            response.json(result)
        }
    })
}
//Mostrará los lugares de las páginas de usuario
export const getadminLugar = (request, response) => {
    //evalua
    connection.query("select * from lugar", (err, result) => {
        if (err) {
            response.json(err)
        } else {
            response.json(result)
        }
    })
}
//----------------------------------------------------------------------------------
//Método POST admin
export const postadminLugar = (request, response) => {
    const { nombre, tipo } = request.body;
    connection.query(
        `INSERT INTO lugar(nombre, tipo) VALUES("${nombre}", "${tipo}")`,
        (err) => {
            if (err) console.log(err);
            response.json('Lugar insertado');
        });
};
//Delete
export const deleteAdminLugar = (request, response) => {
    const { id_L } = request.params
    connection.query(
        ` DELETE FROM lugar WHERE id_L=${id_L}`,
        (err) => {
            if (err) return response.json({ status: false, info: 'El lugar no se pudo eliminar' })
            response.json({ status: true, info: '¡Lugar eliminado!' })
        }
    )

}

