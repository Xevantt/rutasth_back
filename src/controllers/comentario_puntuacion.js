import connection from '../settings/dbConection'

//Llamada a la BDD para mostrar todas las combis en la página del admin de valoraciones
export const getadminCombiValoracion = (request, response) => {
    //evalua
    connection.query("select * from combi", (err, result) => {
        if (err) {
            response.json(err)
        } else {
            response.json(result)
        }
    })
}
//Muestra todos los comentarios en la pagina del admin
export const getComentarios = (request, response) => {
    //evalua
    connection.query("select * from comentario_puntuacion", (err, result) => {
        if (err) {
            response.json(err)
        } else {
            response.json(result)
        }
    })
}
//traerá comentarios de acuerdo del ID con respecto a params
export const getComentariosByID = (request, response) => {
    //evalua
    connection.query(`select * from comentario_puntuacion WHERE id_Co = ${request.params.id_Co}`, (err, result) => {
        if (err) {
            response.json(err)
        } else {
            response.json(result)
        }
    })
}
//------------------------------------------------------------------------------------------------
//Metodo POST
export const postRutasComentario = (request, response) => {
    const { id_Co, nombre_per, comentario } = request.body;
    connection.query(
        `INSERT INTO comentario_puntuacion(id_Co, nombre_per, comentario) VALUES(${id_Co} ,"${nombre_per}", "${comentario}")`,
        (err) => {
            if (err) return response.json({ status: false, info: "¡Caracteres no válidos!", error: err });
            response.json({ status: true, info: 'Comentario agregado' });
        });
};
//Metodo DELETE por medio de params del ID en la página del admin
export const deletecomentariosAdmin = (request, response) => {
    const id_CP = request.params.id_CP
    connection.query(
        `DELETE FROM comentario_puntuacion WHERE id_CP = ${id_CP}`,
        (err) => {
            if (err) console.log(err);
            response.json({ status: true, info: 'Comentario eliminado' });
        });
};
//Realizamos un inner join con las id de la combi con respecto a la tabla de Trayecto.
export const getcomentByID = (request, response) => {
    //evalua
    connection.query(
        `select * from comentario_puntuacion WHERE id_Co = ${request.params.id_Co}`,
        (err, result) => {
            if (err) {
                response.json(err)
            } else {
                response.json(result)
            }
        }
    )

}