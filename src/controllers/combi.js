import connection from '../settings/dbConection'

//Mostrará todas las combis que existen el la BDD
export const getCombi = (request, response) => {
    //evalua
    connection.query("select * from combi", (err, result) => {
        if (err) {
            response.json(err)
        } else {
            response.json(result)
        }
    })
}
//Llamada a la BDD para mostrar todas las combis en la página del admin
export const getadminCombi = (request, response) => {
    //evalua
    connection.query("select * from combi", (err, result) => {
        if (err) {
            response.json(err)
        } else {
            response.json(result)
        }
    })
}
//----------------------------------------------------------------------------------
//Método POST admin
export const postadminCombi = (request, response) => {
    const { numeroC, nombreC, mapa_link } = request.body;
    if (mapa_link == "" || !mapa_link) {
        return response.json({ status: false, info: "Falta la URL" })
    }
    connection.query(
        `INSERT INTO combi(numeroC, nombreC, mapa_link) VALUES(${numeroC}, "${nombreC}", "${mapa_link}")`,
        (err) => {
            if (err) return response.json({ status: false, info: "Combi no insertada", error: err });
            response.json({ status: true, info: 'Combi insertado' });
        });
};
//Realizamos un inner join con las id de la combi con respecto a la tabla de Trayecto.
export const getcombisByLugar = (request, response) => {
    //evalua
    connection.query(
        `SELECT * from trayecto t inner join combi c on t.id_combi=c.id_C where id_origen=${request.params.id_origen} and id_destino=${request.params.id_destino};`,
        (err, result) => {
            if (err) {
                response.json(err)
            } else {
                response.json(result)
            }
        })
}
//Delete
export const deleteAdminCombi = (request, response) => {
    const { id_C } = request.params
    connection.query(
        ` DELETE FROM combi WHERE id_C=${id_C}`,
        (err) => {
            if (err) return response.json({ status: false, info: 'La combi no se pudo eliminar' })
            response.json({ status: true, info: '¡Combi eliminada!' })
        }
    )

}
