import bcrypt from "bcrypt";
import connection from '../settings/dbConection'
import jwt from "jsonwebtoken";
//Para el desarrollo del login o crear usuario se ocupara las siguientes librerías
//npm install bcrypt jsonwebtoken

export const createAccount = async (request, response) => {
    try {
        const { nameCreate, passwordCreate } = request.body; //Recibe los datos del body
        const passwordHash = await bcrypt.hash(passwordCreate, 10) //Encriptacion de la contraseña
        connection.query(
            `INSERT INTO administrador(usuario, password) VALUES('${nameCreate}','${passwordHash}')`,
            (err) => {
                if (err) console.log(err);
                response.json('Cuenta creada');
            })
    } catch (error) { //
        response.status(400).json({ success: false, info: error })
    }
}
//Aquí se lleva acabo el logeo de la cuenta creada o ingresada
export const loginAccount = (request, response) => {
    const { nameLogin, password } = request.body
    connection.query(
        `SELECT * FROM administrador WHERE usuario='${nameLogin}'`,
        async (err, user) => {
            try {
                if (user.length === 0) {
                    response.json({ mensaje: 'Datos erroneos', info: false })//evaluacion del usuario, este objeto lo esta enviando al fronted
                    return
                }
                const userAdmin = user[0]
                const passwordCorrect = await bcrypt.compare(password, userAdmin.password) //"userAdmin.contraseña" userAdmin almacena 3 campos, despues del punto la contraseña va ser declarada igual al campo de la BDD
                if (nameLogin && passwordCorrect) {
                    const userDataToken = {
                        id: userAdmin.id_A,
                        nameLogin: userAdmin.usuario
                    }

                    const token = jwt.sign(userDataToken, 'proccessEncrypt') //proccessEncrypt se puede sustituir por cualquier otra cosa, solo el programador debe saber y esta se utiliza en los MIDDLEWARES

                    response.json({
                        id: userAdmin.id_A,
                        nameLogin: userAdmin.usuario,
                        token
                    })
                } else {
                    response.json({ info: false })//evaluacion de la contraseña, este objeto lo esta enviando al fronted
                }
            } catch (error) {
                console.log(error)
            }

        })
}