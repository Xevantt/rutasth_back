"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.deleteAdminCombi = exports.getcombisByLugar = exports.postadminCombi = exports.getadminCombi = exports.getCombi = void 0;

var _dbConection = _interopRequireDefault(require("../settings/dbConection"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

//Mostrará todas las combis que existen el la BDD
const getCombi = (request, response) => {
  //evalua
  _dbConection.default.query("select * from combi", (err, result) => {
    if (err) {
      response.json(err);
    } else {
      response.json(result);
    }
  });
}; //Llamada a la BDD para mostrar todas las combis en la página del admin


exports.getCombi = getCombi;

const getadminCombi = (request, response) => {
  //evalua
  _dbConection.default.query("select * from combi", (err, result) => {
    if (err) {
      response.json(err);
    } else {
      response.json(result);
    }
  });
}; //----------------------------------------------------------------------------------
//Método POST admin


exports.getadminCombi = getadminCombi;

const postadminCombi = (request, response) => {
  const {
    numeroC,
    nombreC,
    mapa_link
  } = request.body;

  if (mapa_link == "" || !mapa_link) {
    return response.json({
      status: false,
      info: "Falta la URL"
    });
  }

  _dbConection.default.query(`INSERT INTO combi(numeroC, nombreC, mapa_link) VALUES(${numeroC}, "${nombreC}", "${mapa_link}")`, err => {
    if (err) return response.json({
      status: false,
      info: "Combi no insertada",
      error: err
    });
    response.json({
      status: true,
      info: 'Combi insertado'
    });
  });
}; //Realizamos un inner join con las id de la combi con respecto a la tabla de Trayecto.


exports.postadminCombi = postadminCombi;

const getcombisByLugar = (request, response) => {
  //evalua
  _dbConection.default.query(`SELECT * from trayecto t inner join combi c on t.id_combi=c.id_C where id_origen=${request.params.id_origen} and id_destino=${request.params.id_destino};`, (err, result) => {
    if (err) {
      response.json(err);
    } else {
      response.json(result);
    }
  });
}; //Delete


exports.getcombisByLugar = getcombisByLugar;

const deleteAdminCombi = (request, response) => {
  const {
    id_C
  } = request.params;

  _dbConection.default.query(` DELETE FROM combi WHERE id_C=${id_C}`, err => {
    if (err) return response.json({
      status: false,
      info: 'La combi no se pudo eliminar'
    });
    response.json({
      status: true,
      info: '¡Combi eliminada!'
    });
  });
};

exports.deleteAdminCombi = deleteAdminCombi;
//# sourceMappingURL=combi.js.map