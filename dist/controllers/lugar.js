"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.deleteAdminLugar = exports.postadminLugar = exports.getadminLugar = exports.getLugarDestino = exports.getLugarOrigen = void 0;

var _dbConection = _interopRequireDefault(require("../settings/dbConection"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

//Mostrar los lugares de la categoria origen
const getLugarOrigen = (request, response) => {
  //evalua
  _dbConection.default.query("select * from lugar where tipo like 'O' order by nombre asc", (err, result) => {
    if (err) {
      response.json(err);
    } else {
      response.json(result);
    }
  });
}; //Mostrar los lugares de la categoria destino


exports.getLugarOrigen = getLugarOrigen;

const getLugarDestino = (request, response) => {
  //evalua
  _dbConection.default.query("select * from lugar where tipo like 'D' order by nombre asc", (err, result) => {
    if (err) {
      response.json(err);
    } else {
      response.json(result);
    }
  });
}; //Mostrará los lugares de las páginas de usuario


exports.getLugarDestino = getLugarDestino;

const getadminLugar = (request, response) => {
  //evalua
  _dbConection.default.query("select * from lugar", (err, result) => {
    if (err) {
      response.json(err);
    } else {
      response.json(result);
    }
  });
}; //----------------------------------------------------------------------------------
//Método POST admin


exports.getadminLugar = getadminLugar;

const postadminLugar = (request, response) => {
  const {
    nombre,
    tipo
  } = request.body;

  _dbConection.default.query(`INSERT INTO lugar(nombre, tipo) VALUES("${nombre}", "${tipo}")`, err => {
    if (err) console.log(err);
    response.json('Lugar insertado');
  });
}; //Delete


exports.postadminLugar = postadminLugar;

const deleteAdminLugar = (request, response) => {
  const {
    id_L
  } = request.params;

  _dbConection.default.query(` DELETE FROM lugar WHERE id_L=${id_L}`, err => {
    if (err) return response.json({
      status: false,
      info: 'El lugar no se pudo eliminar'
    });
    response.json({
      status: true,
      info: '¡Lugar eliminado!'
    });
  });
};

exports.deleteAdminLugar = deleteAdminLugar;
//# sourceMappingURL=lugar.js.map