"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.getnombreMapa = exports.getMapa = void 0;

var _dbConection = _interopRequireDefault(require("../settings/dbConection"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

//Esta sección va en la pagina rutas de la página principal.
//Este proceso se reflejará en la página del frontend de la sección de rutas    onclick="getRutaMapa(mostrarRL)"
//Aquí traera los url de los mapas de acuerdo a las combis
const getMapa = (request, response) => {
  //evalua
  _dbConection.default.query(`select * from combi WHERE id_C=${request.params.id_C}`, (err, result) => {
    if (err) {
      response.json(err);
    } else {
      response.json(result);
    }
  });
}; //Aquí traera el nombre de los mapas de acuerdo a las combis del INDEX


exports.getMapa = getMapa;

const getnombreMapa = (request, response) => {
  //evalua
  _dbConection.default.query(`select * from combi WHERE id_C=${request.params.id_C}`, (err, result) => {
    if (err) {
      response.json(err);
    } else {
      response.json(result);
    }
  });
};

exports.getnombreMapa = getnombreMapa;
//# sourceMappingURL=mapa.js.map