"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.loginAccount = exports.createAccount = void 0;

var _bcrypt = _interopRequireDefault(require("bcrypt"));

var _dbConection = _interopRequireDefault(require("../settings/dbConection"));

var _jsonwebtoken = _interopRequireDefault(require("jsonwebtoken"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

//Para el desarrollo del login o crear usuario se ocupara las siguientes librerías
//npm install bcrypt jsonwebtoken
const createAccount = async (request, response) => {
  try {
    const {
      nameCreate,
      passwordCreate
    } = request.body; //Recibe los datos del body

    const passwordHash = await _bcrypt.default.hash(passwordCreate, 10); //Encriptacion de la contraseña

    _dbConection.default.query(`INSERT INTO administrador(usuario, password) VALUES('${nameCreate}','${passwordHash}')`, err => {
      if (err) console.log(err);
      response.json('Cuenta creada');
    });
  } catch (error) {
    //
    response.status(400).json({
      success: false,
      info: error
    });
  }
}; //Aquí se lleva acabo el logeo de la cuenta creada o ingresada


exports.createAccount = createAccount;

const loginAccount = (request, response) => {
  const {
    nameLogin,
    password
  } = request.body;

  _dbConection.default.query(`SELECT * FROM administrador WHERE usuario='${nameLogin}'`, async (err, user) => {
    try {
      if (user.length === 0) {
        response.json({
          mensaje: 'Datos erroneos',
          info: false
        }); //evaluacion del usuario, este objeto lo esta enviando al fronted

        return;
      }

      const userAdmin = user[0];
      const passwordCorrect = await _bcrypt.default.compare(password, userAdmin.password); //"userAdmin.contraseña" userAdmin almacena 3 campos, despues del punto la contraseña va ser declarada igual al campo de la BDD

      if (nameLogin && passwordCorrect) {
        const userDataToken = {
          id: userAdmin.id_A,
          nameLogin: userAdmin.usuario
        };

        const token = _jsonwebtoken.default.sign(userDataToken, 'proccessEncrypt'); //proccessEncrypt se puede sustituir por cualquier otra cosa, solo el programador debe saber y esta se utiliza en los MIDDLEWARES


        response.json({
          id: userAdmin.id_A,
          nameLogin: userAdmin.usuario,
          token
        });
      } else {
        response.json({
          info: false
        }); //evaluacion de la contraseña, este objeto lo esta enviando al fronted
      }
    } catch (error) {
      console.log(error);
    }
  });
};

exports.loginAccount = loginAccount;
//# sourceMappingURL=administrador.js.map