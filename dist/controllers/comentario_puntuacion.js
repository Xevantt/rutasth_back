"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.getcomentByID = exports.deletecomentariosAdmin = exports.postRutasComentario = exports.getComentariosByID = exports.getComentarios = exports.getadminCombiValoracion = void 0;

var _dbConection = _interopRequireDefault(require("../settings/dbConection"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

//Llamada a la BDD para mostrar todas las combis en la página del admin de valoraciones
const getadminCombiValoracion = (request, response) => {
  //evalua
  _dbConection.default.query("select * from combi", (err, result) => {
    if (err) {
      response.json(err);
    } else {
      response.json(result);
    }
  });
}; //Muestra todos los comentarios en la pagina del admin


exports.getadminCombiValoracion = getadminCombiValoracion;

const getComentarios = (request, response) => {
  //evalua
  _dbConection.default.query("select * from comentario_puntuacion", (err, result) => {
    if (err) {
      response.json(err);
    } else {
      response.json(result);
    }
  });
}; //traerá comentarios de acuerdo del ID con respecto a params


exports.getComentarios = getComentarios;

const getComentariosByID = (request, response) => {
  //evalua
  _dbConection.default.query(`select * from comentario_puntuacion WHERE id_Co = ${request.params.id_Co}`, (err, result) => {
    if (err) {
      response.json(err);
    } else {
      response.json(result);
    }
  });
}; //------------------------------------------------------------------------------------------------
//Metodo POST


exports.getComentariosByID = getComentariosByID;

const postRutasComentario = (request, response) => {
  const {
    id_Co,
    nombre_per,
    comentario
  } = request.body;

  _dbConection.default.query(`INSERT INTO comentario_puntuacion(id_Co, nombre_per, comentario) VALUES(${id_Co} ,"${nombre_per}", "${comentario}")`, err => {
    if (err) return response.json({
      status: false,
      info: "¡Caracteres no válidos!",
      error: err
    });
    response.json({
      status: true,
      info: 'Comentario agregado'
    });
  });
}; //Metodo DELETE por medio de params del ID en la página del admin


exports.postRutasComentario = postRutasComentario;

const deletecomentariosAdmin = (request, response) => {
  const id_CP = request.params.id_CP;

  _dbConection.default.query(`DELETE FROM comentario_puntuacion WHERE id_CP = ${id_CP}`, err => {
    if (err) console.log(err);
    response.json({
      status: true,
      info: 'Comentario eliminado'
    });
  });
}; //Realizamos un inner join con las id de la combi con respecto a la tabla de Trayecto.


exports.deletecomentariosAdmin = deletecomentariosAdmin;

const getcomentByID = (request, response) => {
  //evalua
  _dbConection.default.query(`select * from comentario_puntuacion WHERE id_Co = ${request.params.id_Co}`, (err, result) => {
    if (err) {
      response.json(err);
    } else {
      response.json(result);
    }
  });
};

exports.getcomentByID = getcomentByID;
//# sourceMappingURL=comentario_puntuacion.js.map