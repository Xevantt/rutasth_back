"use strict";

var _comentario_puntuacion = require("../controllers/comentario_puntuacion");

var _decodeUser = _interopRequireDefault(require("../middlewares/decodeUser"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

module.exports = app => {
  app.get('/get/adminCombi/Valoracion', _comentario_puntuacion.getadminCombiValoracion);
  app.get('/get/comentario/all', _comentario_puntuacion.getComentarios);
  app.get('/get/ComentariosByID/:id_Co', _comentario_puntuacion.getComentariosByID);
  app.post('/post/rutasComentario', _comentario_puntuacion.postRutasComentario);
  app.delete('/delete/comentariosAdmin/:id_CP', _decodeUser.default, _comentario_puntuacion.deletecomentariosAdmin);
  app.get('/get/comentByID/:id_Co', _comentario_puntuacion.getcomentByID);
};
//# sourceMappingURL=comentario_puntuacion.js.map