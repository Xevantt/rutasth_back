"use strict";

var _mapa = require("../controllers/mapa");

module.exports = app => {
  app.get('/get/mapa/all/:id_C', _mapa.getMapa);
  app.get('/get/nombreMapa/:id_C', _mapa.getnombreMapa);
};
//# sourceMappingURL=mapa.js.map