"use strict";

var _combi = require("../controllers/combi");

var _decodeUser = _interopRequireDefault(require("../middlewares/decodeUser"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

module.exports = app => {
  app.get('/get/combi/all', _combi.getCombi);
  app.get('/get/adminCombi', _combi.getadminCombi);
  app.post('/post/postadminCombi', _decodeUser.default, _combi.postadminCombi);
  app.get('/get/combiByLugar/:id_origen/:id_destino', _combi.getcombisByLugar);
  app.delete('/delete/adminCombi/:id_C', _decodeUser.default, _combi.deleteAdminCombi);
};
//# sourceMappingURL=combi.js.map