"use strict";

var _administrador = require("../controllers/administrador");

module.exports = app => {
  //Creacion y logeo de cuentas
  app.post('/post/createAccount/admin', _administrador.createAccount);
  app.post('/post/loginAccount/admin', _administrador.loginAccount);
};
//# sourceMappingURL=administrador.js.map