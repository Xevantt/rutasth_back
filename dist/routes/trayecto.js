"use strict";

var _trayecto = require("../controllers/trayecto");

var _decodeUser = _interopRequireDefault(require("../middlewares/decodeUser"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

module.exports = app => {
  app.post('/post/adminTrayecto', _decodeUser.default, _trayecto.postadminTrayecto);
  app.get('/get/adminLugar/Origen', _trayecto.getadminLugarOrigen);
  app.get('/get/adminLugar/Destino', _trayecto.getadminLugarDestino);
  app.get('/get/adminCombi/Trayecto', _trayecto.getadminCombiTrayecto);
  app.get('/get/adminIDtrayecto/all', _trayecto.getadminIDtrayecto);
  app.delete('/delete/adminTrayecto/:id', _decodeUser.default, _trayecto.deleteAdminTrayecto);
};
//# sourceMappingURL=trayecto.js.map