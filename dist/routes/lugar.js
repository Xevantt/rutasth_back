"use strict";

var _lugar = require("../controllers/lugar");

var _decodeUser = _interopRequireDefault(require("../middlewares/decodeUser"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

module.exports = app => {
  app.get('/get/lugarOrigen', _lugar.getLugarOrigen);
  app.get('/get/lugarDestino', _lugar.getLugarDestino);
  app.get('/get/adminLugar', _lugar.getadminLugar);
  app.post('/post/postadminLugar', _decodeUser.default, _lugar.postadminLugar);
  app.delete('/delete/adminLugar/:id_L', _decodeUser.default, _lugar.deleteAdminLugar);
};
//# sourceMappingURL=lugar.js.map