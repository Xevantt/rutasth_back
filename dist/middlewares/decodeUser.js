"use strict";

var _jsonwebtoken = _interopRequireDefault(require("jsonwebtoken"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

//decodeUser - decodificar al usuario para evitar cambiar valores si no tiene el token
module.exports = (request, response, next) => {
  if (!request.get('authorization')) {
    response.json({
      status: false,
      info: "No cuenta con los permisos, acceda primero."
    });
    return;
  }

  const authorization = request.get('authorization');
  let token = '';

  if (authorization && authorization.toLowerCase().startsWith('bearer')) {
    token = authorization.substring(7);
  }

  const decodeToken = _jsonwebtoken.default.verify(token, 'proccessEncrypt'); //proccessEncrypt es la llave del programador y esta se utiliza en los controllers administrador "loginAccount"


  if (!token || !decodeToken.id) {
    return response.status(401).json({
      success: false,
      info: 'token faltante o inválido'
    });
  }

  next();
};
//# sourceMappingURL=decodeUser.js.map